#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 14:07:05 2020

@author: s19000947 (SAIDI Somia)
"""
import math

''' A Définissez une classe Polygone comportant la variable membre:
•sommets – liste contenant les sommets du polygone et les méthodes:
•__init__(self, sommets) – construction à partir de la liste de ses sommets 
•getSommet(self, i) – obtention du ième sommet du polygone 
•aire(self) – calcul de la surface du polygone (voir indications ci-dessous) 
•__str__(self)  – expression textuelle du polygone; par exemple  "[(1.0, 1.0),(5.0, 1.0), (3.0, 2.0)]" '''

from points import Point

class Polygone:
    def __init__(self, sommets):
        self.sommets = sommets
      
    def getSommet(self, i): 
        return self.sommets[i]
    
    def aire(self):
        ''' calcul de la surface du polygone (voir indications ci-dessous)'''
        S = 0
        for i in range(len(self.sommets)-1):
           S = S + (self.sommets[i].x() + self.sommets[i+1].x()) * (self.sommets[i].y() + self.sommets[i+1].y())
        S = 0.5*S
        return S
    
    def __str__(self):
        string = "["
        for i in range(len(self.sommets)):
            string = string + str(self.sommets[i]) + ", "
        return string + "]"

print("Partie A: classe Polygone")
A = Point(1,3)
B = Point(1,4)
C = Point(2,3)
D = Point(5,4)
P = Polygone([A,B,C,D])
print(P.getSommet(1))
print(P.aire())
print(P)

# B : Définissez une classe Triangle, sous-classe de Polygone,

class Triangle(Polygone):
    '''Construction du triangle ayant les sommets indiqués'''
    def __init__(self, a, b, c):
        super().__init__([a, b, c])
#        self.a = a
#        self.b = b
#        self.c = c
        
# C Définissez une classe Rectangle (sous-entendu, avec des côtés parallèles aux axes), sous-classe de Polygone, munie d’un constructeur
# •__init__(self, xMin, xMax, yMin, yMax)  – construction d’un rectangleparallèle aux axes
        
class Rectangle(Polygone):
    '''Construction d’un rectangle parallèle aux axes'''
    def __init__(self, xMin, xMax, yMin, yMax):
        super().__init__([xMin, xMax, yMin, yMax])
#        self.xMin = xMin
#        self.xMax = xMax
#        self.yMin = yMin
#        self.yMax = yMax

# D Définissez une classe  PolygoneRegulier, sous-classe de  Polygone, disposant d’unconstructeur•
# __init__(centre,   rayon,   nombreSommets)  –   construction   du   polygonerégulier ayant le centre, le rayon et le nombre de sommets indiqués. 
           
class PolygoneRegulier(Polygone):
    def __init__(self, centre, rayon, nombreSommets):
        super().__init__([])
        # les arguments des constructeurs ne deviennent pas forcément des variables d'instance
        # on peut avoir des arg qui vont permettre de calculer des variables d'instance, cas ici de centre, rayon, nombreSommets
        for i in range(nombreSommets):
            x1 = centre.x() + rayon * math.cos(2*math.pi*i/nombreSommets)
            y1 = centre.y() + rayon * math.sin(2*math.pi*i/nombreSommets)
            self.sommets.append(Point(x1,y1))
# E Écrivez dans un autre fichier un programme de test construisant et affichant des polygones dediverses sortes et permettant de vérifier que la méthode aire est correcte.
        
print("Polygone régulier")
PR = PolygoneRegulier(Point(1,1),5,8)
print(PR)

print("Triangle")
Tr = Triangle(A,B,C)
print(Tr)

print("Rectangle")
rect = Rectangle(A,B,C,D)
print(rect)
